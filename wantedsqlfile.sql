-- MySQL dump 10.13  Distrib 5.7.25, for Linux (x86_64)
--
-- Host: localhost    Database: demo_db
-- ------------------------------------------------------
-- Server version	5.7.25-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `demo_table`
--
CREATE DATABASE IF NOT EXISTS iTR26SxUTP CHARACTER SET =utf8mb4 COLLATE=utf8mb4_unicode_ci;
DROP TABLE IF EXISTS `demo_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `demo_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email_id` varchar(255) DEFAULT NULL,
  `message` varchar(400) DEFAULT NULL,
  `date_time` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `demo_table`
--

LOCK TABLES `demo_table` WRITE;
/*!40000 ALTER TABLE `demo_table` DISABLE KEYS */;
INSERT INTO `demo_table` VALUES (1,'dff','dff2v@dsfs.ddd','sdfsdfdsdf fdfd\r\n','2019-02-20 20:38:33.289'),(2,'alok','aniket@nsit.dsdd','alok is great','2019-02-21 23:50:49.600'),(3,'dff','fsdf@dfsf.sd','sadsnf','2019-02-22 00:16:48.136'),(4,'alok','ssdd@fdf.com','dsdf','2019-02-24 22:37:55.373'),(5,'alok','ssdd@fdf.com','dsdf','2019-02-24 22:44:00.168'),(6,'Sj','sj@hi2.in','Testing','2019-02-25 21:09:16.978'),(7,'hahd','hshhs@shhs.com','Sbhdhf','2019-02-25 21:54:18.622'),(8,'dsfdf','sddff@dfd.d','fdsffd\r\n','2019-03-01 00:30:49.350'),(9,'alok verma','sddfsf2@sef.com','ddsfs s sa d','2019-03-11 11:38:21.076'),(10,'dxf','sad@sdsf.cd','sf sd\r\nf\r\n','2019-03-11 11:41:01.579'),(11,'alpk','dsf2efe#@sd.fdd','sdffdf','2019-03-14 20:07:10.940');
/*!40000 ALTER TABLE `demo_table` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-03-22 11:27:53
